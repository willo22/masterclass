# TP4 

## Le remote 


Vous allez commencer par créer un compte sur gitlab : 

![](Images/CompteGitLab1.png)

![](./Images/compteGitLab2.png)

![](./Images/compteGitLab3.png)

Si vous lisez le Readme de votre nouveau projet vous trouverez les indication pour pousser votre dépot d'exercice.

![](./Images/gitlab3.png)




Pour connecter votre dépot local au dépot distant, celui que vous venez de créer sur le site GitLab, utiliser les commandes  : 

```
git remote add origin <https://gitlab.com/exo6/tpgit>
```

puis pour vérfier : 
```
git remote -v
```

Renommer votre branch Master en Main (la branche par défaut de gitlab est Main...)

git branch -M main

Enfin pour pousser vos fichiers vers le déport distant utiliser : 

```
git push origin main
```

Origin correspond au nom du dépot distant.
master correspond à la branche que vous souhaitez pousser.


Si vous obtenez une erreur : 

! [remote rejected] main -> main (pre-receive hook declined)

![](./Images/gitlab4.png)
![](./Images/gitlab5.png)

Pour voir l'état d'avancement des dépots local et distant, vous pouvez utiliser la commande 
```
git log --oneline
```

faites une modification sur index.hml et commiter. 

regarder l'état des depots distant et locaux. 

Poussez votre modification avec la commande 
```
git push origin main
```
regarder de nouveau l'état des dépots



## Clonner un repository

Pour cloner un dépot, vous pouvez utiliser la commande : 

git clone URlDuDepot.


Par exemple avec la commande : 
```
git clone https://gitlab.com/willo22/masterclass.git
```

vous récupérez les sources et pdf de cette masterclass.















