# Mémo demo basic git

**Note a préciser** : ligne de comande, elle permet de comprendre ce que l'on fait + nécessaire en cas de pb/conflit

## Création du dépot : 

- git init

config obligatoire : 

- git config --global user.email "ssr@glaizot.net"
- git config --global user.name "Maxime -- formateur"

trouver de l'aide sur git : 
- git help config
  
voire la config : 
- git config --list 



## Premier Commit 

- git status
- git add LesFichiers
- git commit -m "message"

supprimer un fichier ajouter avec add : 
- git rm --cached Lefichier

Ignorer des fichiers : création d'un fichier .gitignore

## consulter l'historique des modifications

- git log 
- git help log pour l'aide

Voire les modif faire sur un fichier dans le différent commut : 
- git log -p CheminVersLeFichier 
- git diff voire les modif entre notre version de w et le dernier commit



## retour en arrière 


## Branch : 
Création d'une branche :
git branch laBranche

passer dans une branche : 
git checkout laBranche

Pour merger les branches : 
git checkout master 
git merge Work 

supprimer branche local : 
git branch -d LaBranche

git push origin --delete LaBranche 



## retour en arrière

git log --oneline -p suite.html
git checkout 9aeefab

git checkout 9aeefab suite.html
git status 
git log --oneline 
git commit -m "reparation des bétises"

git revert e4eda3b

git revert --abort 

git revert HEAD~3..HEAD

git reset 45f7e5a




## acces au remote : 

git remote add origin LienVersLeDépot
git push origin master

pusher une branch :
git push origin laBranche


git remote -v afficheir les remote




