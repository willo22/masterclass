# TP 1

## Installation de GIT sur votre PC

Nous allons installer le logiciel Git sur notre session locale Windows de votre PC.

Il existe plusieurs manières d’installer Git sur Windows. 
L’application officielle est disponible au téléchargement sur le site web de Git. 

Rendez-vous sur https://gitforwindows.org/ pour le télécharger.

Notez que ce projet est nommé Git for Windows

### Options d'installations des composants Git:

Cochez l'option d'installation **Git Bash**  (Ceci nous permettra d'utiliser Git via PowerShell ou la CMD ou encore un émulateur Git Bash)

![center](./Images/capture_install_git_01.png)

Il faudra penser à selectionner et/ou installer un éditeur de code de votre choix si vous n'en disposez pas.

![center](./Images/capture_install_git_02.png)

Si vous ne disposez d'aucun éditeur de code comme Notepad++ ou VSCode, 
**Télécharger celui de votre choix en fonction de la liste, puis installer l'éditeur de code que vous avez choisi avant de poursuivre l'installation de Git.**
(Si besoin, revenir en arrière, puis selectionner dans la liste, l'éditeur de texte que vous avez installé, l'option Next n'est plus grisée)

![center](./Images/capture_install_git_03.png)

Choix de la branch par défaut (master), laisser Git décider pour vous.

![center](./Images/capture_install_git_04.png)

Utilisez les options recommandées pour intégrer l'environnement des commandes Git au PATH de la console Windows ou PowerShell.

![center](./Images/capture_install_git_05.png)

Utilisez l'option OpenSSH pour pouvoir vous connectez plus tard à un dépôt GitLab ou GitHub distant.

![center](./Images/capture_install_git_06.png)

Choisir l'option permettant de convertir le format d'encode du texte Windows en Unix et vis-versa.

![center](./Images/capture_install_git_07.png)

Choisir l'option MiniTTY comme emulator Git Bash, nous ne serons pas impacté, nous utiliserons PowerShell, CMD ou Terminal Windows dispo sur le Store Microsoft.

![center](./Images/capture_install_git_08.png)

Choisir l'option Default pour merger les branch

![center](./Images/capture_install_git_09.png)


Choisir l'option Git Credential Manager

![center](./Images/capture_install_git_10.png)

Pour la suite, laisser les options affichées par défaut puis Next jusqu'à la fin de l'installateur.

Pour verifier que Git fonctionne, ouvrir votre terminal CMD ou PowerShell, puis tapez la commande **git version**:

```
git version

git version 2.30.0.windows.2
```

Pour connaitre les commandes de base, tapez la commande **git help**:

```
git help

usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

These are common Git commands used in various situations:

start a working area (see also: git help tutorial)
   clone             Clone a repository into a new directory
   init              Create an empty Git repository or reinitialize an existing one

work on the current change (see also: git help everyday)
   add               Add file contents to the index
   mv                Move or rename a file, a directory, or a symlink
   restore           Restore working tree files
   rm                Remove files from the working tree and from the index
   sparse-checkout   Initialize and modify the sparse-checkout

examine the history and state (see also: git help revisions)
   bisect            Use binary search to find the commit that introduced a bug
   diff              Show changes between commits, commit and working tree, etc
   grep              Print lines matching a pattern
   log               Show commit logs
   show              Show various types of objects
   status            Show the working tree status

grow, mark and tweak your common history
   branch            List, create, or delete branches
   commit            Record changes to the repository
   merge             Join two or more development histories together
   rebase            Reapply commits on top of another base tip
   reset             Reset current HEAD to the specified state
   switch            Switch branches
   tag               Create, list, delete or verify a tag object signed with GPG

collaborate (see also: git help workflows)
   fetch             Download objects and refs from another repository
   pull              Fetch from and integrate with another repository or a local branch
   push              Update remote refs along with associated objects

```

## Création du dépot Local: 

Créez un repertoire et initialisez un premier depot Git dans ce répertoire.
Sur Windows à la racine du C:/
Sur Linux dans votre /root

```
cd C:/
mkdir projetmasterclass
cd projetmasterclass
git init
git config --global user.email "votreEmail"
git config --global user.name "votreNom"
git config --list 

```

affichez l'aide de la commande config : 

```
git help config
```

## Premier Commit 

- Créez un fichier index.html dans le repertoire de votre projet

- Déroulez les commandes suivantes et notez ce qu'il se passe :  
Sur Windows créer un fichier texte index.html avec un éditeur de texte comme VScode dans le dossier projectmasterclass/git
Sur Linux avec nano index.html

```
git status 
git add index.html
git status
git commit -m "Premier Commit, ajout du fichier index.html"
git status
```

## On continue !

- modifiez le contenu de votre fichier index.html
- créez un fichier note.txt 
- vérifiez le status
- mettre en "staging" les fichiers note.html et index.html

- finalement, vous ne voulez pas faire de commit du fichier note.txt, pour l'enlever du "staging" utiliser la commande suivante : 

```bash
git rm --cached note.txt

ou bien la commande que vous indique git : 

git restore --staged
```
- vérifiez à nouveau le status
- faites un commit

- vous décidez d'ignorer tout les fichiers avec l'extension .txt, pour cela créer un fichier .gitignore dans votre repertoire dont le contenu sera : 

```bash
*.txt
```

- faites de nouveau un statut. 
- Y a t'il un fichier à ajouter à votre dépot ? si c'est le cas, faites le nécéssaire pour le commiter

- ajoutez un nouveau fichier (ex suite.html) et commiter le.  


## Consulter l'historique des modifications

- Vous pouvez consulter l'historique des modifications avec les commande suivante : 

```
git help log 
git log 
git log --oneline
```

- Executez la commande suivante pour voir les modifications effectuées sur le fichier index.html vous éxécutez la commande suivante : 
```
git log -p index.html 
```

- Modifiez une nouvelle fois votre fichier index.html en ajoutant une ligne à la fin. 
- utilisez la commande suivante pour voir les modifications de votre fichier par rapport au dernier commit 

```bash
git diff
```
- A partir des informations fournies par le git status, trouvez comment annuler les modifications réalisées sur index.html et revenir à la dernière version commitée


