---
marp: true
theme: gaia
markdown.marp.enableHtml: true
class:
-   invert


<!-- $theme: gaia -->
---
<style>

img[alt~="center"] {
  display: block;
  margin: 0 auto;
}

</style>


# Introduction a GIT

---
## Vos formateurs

#### Maxime Glaizot
Ingénieur système et réseau et formateur
email : formation-ssr@glaizot.net
LinkedIn : Maxime Glaizot

#### BRUSSE Rémi
Ingénieur Systèmes, Réseaux, Cloud et Cybersécurité
Consultant en sécurité du SI et vDSI





---
# Objectifs 

- Découvrir Git
- Vous incitez à l'utiliser dans votre quotidien
- Découvrir quelques pièges

---
# Gestionnaire de version ? 

### Outils permettant d'**enregistrer**, de **suivre** et de **gérer** plusieurs versions d'un fichier (Config, doc, script, code source....)

---
# Gestionnaire de versions ?

- ### **Objectif 1** : Garder l'historique des différentes mises à jour
- ### **Objectif 2** : Faciliter le travail à plusieurs.

---
# Gestionaire de versions
## Pourquoi faire ?

- Gestion de vos documentations
- Gestion des fichiers de configurations
- Gestion de vos scripts
- ...

---
# Gestionaire de versions
## Pour aller plus loin

Utilisation dans un pipeline CI/CD

![center](./Images/cicd_pipeline_infograph.png)


---
# quelques outils (libres)

- #### **CVS** : Concurent Version System (le plus "ancien") 
- #### **SVN** : Subversion, le concurent de CVS
- #### **GIT** : Notre sujet du jour
- #### **Mercurial** : des concurent de GIT 
- #### ...
  
<!-- Puig s’appuie sur le Protocole ICMP ( Internet Control Message Protocol ) qui envoie un paquet
« echo request » à la machine cible. si cette dernière reçoit les paquets « echo reply », elle affichera un certaine message indiquant que le destinataire est bien joignable :
Nb noeud traversé : 127 - TTL
 -->


---
# quelques outils (Propriétaires)

- #### ClearCase (IBM)
- #### Synergy (IBM)
- #### Visual Source Safe (Microsoft)
- #### AlienBrain, spécialiser dans la gestion d'images et de sons
- #### BitKeeper (On en reparlera... ;) )
- #### ...

--- 
# NE PAS CONFONDRE

![center height:400px](./Images/Git-vs-github.png)

---
# La petite histoire de Git

![height:400 center](./Images/800px-LinuxCon_Europe_Linus_Torvalds_03_(cropped).jpg)


---
# Git les concepts

- gestionnaire de versions distribuées
- multiplateforme !
- Dépôt/Répository : espace de stocckage géré par git
- Commit : Valider des modifications dans notre dépot
- push : Propager les modifications vers un dépot distant
- pull : récupérer la dernière version d'un dépot distant
- Clone : recopier en local un dépot distant



--- 
## Git les concepts

![height:550px center](Images/git-repos.png)


--- 
# Working directory

- votre espace de travail
- par defaut les modifications ne seront pas prise en compte dans le prochain commit
- *git add* pour prendre en compre les modifications (stagged)
- Un fichier peut être suivit ou non suivit

--- 
# Commit

- valider une série de modifications 
- Créer une "version" 
- un commit possède un ID
- un commit possède un message de log

---
# stagging area

- espace "temporaire" pour indiquer la liste des fichiers qui feront partie du prochain commit


---
# dépot distant 

- gestionnaire de versions distribuées
- copie de notre dépot local, destiné a être partagé.
- copier local vers distant  =  **PUSH**
- copier de distant vers local = **PULL**

### **Il faut toujours être a jour avant de faire un PUSH**


--- 
## Git les concepts

![height:550px center](Images/git-repos.png)



