# TP 3

## Arrière toute !

Avant de commencer cette partie, noter l'identifiant du dernier commit effectué. 


Ajouter une nouvelle ligne au fichier index.html. 
Vérifier les modifications entre le dernier commit et votre version de travail 

Revennez à la version du dernier commit avec la commande : 

```
git restore index.html
```

Editez le fichier index.html et effacez tout son contenu avant de faire ne commit. 
Créez un fichier index2.html que vous commiterez aussi. 

Vous vous rendez compte qu'il ne fallait pas effacer le contenu de index.html. Pour retrouver ou a eu lieu la modification, utiliser la commande : 
```
git log --oneline  -p index.html
```

quand vous avez identifiez le commit responsable de la suppression, vous pouvez l'annuler avec la commande (ou  cb9ed77 est l'identifiant du commit à annuler): 

```
git revert cb9ed77
```

vous constaterez que cela n'a pas supprimé le fichier index2.html que vous avec commité en dernier. 

Regardez les log, vous verrez apparaitre un nouveau commit


Si vous souhaitez revenir de trois commit en arrière, vous pouvez utiliser la commande suivante : 
```
git revert HEAD~3..HEAD
```

Vérifier les log (git log --oneline), que constatez-vous ?


Si vous souhaitez consulter l'état de votre dépot tel qu'il était au début de ce TP, utiliser la commande (avec 12f1db6 l'id noté en début de TP):

```
git checkout 12f1db6
```

Lisez bien le message d'avertissement. Vous êtes simplement en "consultation" d'un commit. En l'état, vous ne pouvez rien modifier sans créer une nouvelle branche. 

Pour revenir dans le mode de fonctionnement classique, utilisez la commande 

```
git switch - 
```

Pour revenir de manière **définitive** au commit du début de ce TP utilisez la commande : 

```
git reset 12f1db6
```

