# TP 2

## Navigons de branche en Branche

La branche par défaut est nomée master

Pour créer une nouvelle branche "Work" passer la commande suivante : 

```
git branch Work
```
la commande suivante vous permet de lister les branch : 
```
git branch 
```

utiliser la commande status pour voir quelle est la branche active.

Pour passer dans la branche Work, utiliser la commande : 

```
git switch Work
```

la commande log vous permet de voir l'état de vos branches : 

```
git log --oneline 

d80ecd2 (HEAD -> Work, master) ajout de suite.html
847ef63 ajout de .gitignore
99831ea deuxieme commit
d02e623 Premier Commit, ajout du fichier index.html
```

modifier le fichier index.html en ajoutant une nouvelle ligne, puis faire un commit (en vous assurant que vous êtes bien dans la branche Work avant de procéder au Commit)

si vous consulter l'état des commit de nouveau (avec la commande log) vous devriez constater un décalage entre votre branche master et Work comme ci dessous :

```
d0c83e5 (HEAD -> Work) ajout 2nd ligne index.html
d80ecd2 (master) ajout de suite.html
847ef63 ajout de .gitignore
99831ea deuxieme commit
d02e623 Premier Commit, ajout du fichier index.html
```

créez le fichier log.txt, "stagge" avant de faire un commit.
Vous devriez obtenir une erreur. Le message devrait vous permettre de comprendre ce qu'il se passe. 
Trouvez comment commiter le fichier log.txt avec l'aide obtenue dans l'erreur.

Nous avons fini le travail sur la branch Work, nous allons fusionner la branch Work avec la branche Master grâce à la commande (merge). 
Pour cela, repasser dans la branch master avec la commande : 

```
git switch master
```
procédé au merge avec la commande (ou Work correpond a la branche que vous souhaitez intégré dans la branche Master) : 

```
git merge Work
```

enfin, nous allons supprimer la branche Work grâce à la commande suivante : 

```
git branch -d Work
```

## A vous de jouer ! 

Il vous est demandé d'ajouter une ligne au fichier index.html ainsi qu'au fichier suite.html. Vous ferrez un commit entre chaque modification.
Pour permettre la validation de votre travail, utiliserez une branche nomée Dev. Une fois les modifications validées sur votre branche, vous ferrez un merge et supprimerez la branche Dev 


